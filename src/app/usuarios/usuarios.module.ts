import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablaUsuariosComponent } from './tabla-usuarios/tabla-usuarios.component';
import { UsuariosService } from './usuarios.service';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner'
import { MatTableModule } from '@angular/material/table' 



@NgModule({
  declarations: [
    TablaUsuariosComponent
  ],
  exports:[
    TablaUsuariosComponent
  ],
  providers:[UsuariosService],
  imports: [
    CommonModule,
    MatProgressSpinnerModule,
    MatTableModule
  ],
})
export class UsuariosModule { }
