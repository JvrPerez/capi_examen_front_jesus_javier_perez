import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class UsuariosService {

  constructor(private httpClient: HttpClient) { }

  public baseUrl = "http://localhost:8000";
  public getUsers(){
    return this.httpClient.get(`${this.baseUrl}/users`)
  }
}
