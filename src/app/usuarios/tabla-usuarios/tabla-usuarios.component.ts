import { Component, OnInit, ViewChild } from '@angular/core';
import { UsuariosService } from '../usuarios.service';
import {MatTableDataSource} from '@angular/material/table';


@Component({
  selector: 'app-tabla-usuarios',
  templateUrl: './tabla-usuarios.component.html',
  styleUrls: ['./tabla-usuarios.component.scss']
})
export class TablaUsuariosComponent implements OnInit {

  constructor( private userService: UsuariosService) { }

  users:any = []

  dataUsers:User[] = []
  dataSource:any
  
  displayedColumns: string[] = ['nombre', 'fecha_nacimiento', 'edad', 'domicilio'];

  ngOnInit(): void {
    this.userService.getUsers().subscribe(respuesta=>{
      this.users = respuesta

      for(let item in this.users) {
        console.log(this.users[item].domicilio);

        let user = this.users[item]
        let domicilio = this.users[item].domicilio

        this.dataUsers.push({
          name:user.name,
          fecha_nacimiento:user.fecha_nacimiento,
          edad:user.edad,
          domicilio:`#${domicilio.domicilio}, CP. ${domicilio.cp}, ${domicilio.ciudad}`,
        }) 
      };
      
      this.dataSource = new MatTableDataSource<User>(this.dataUsers)
    })
  }


}

export interface User {
  name:string,
  fecha_nacimiento:string,
  edad:number,
  domicilio:string
}
